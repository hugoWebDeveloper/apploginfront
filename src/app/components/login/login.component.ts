import { Component, OnInit } from '@angular/core';
import { LoginService, Usuario } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string;
  contrasena: string;

  constructor(
    private loginService: LoginService
  ) { }

  ngOnInit() {
  }

  iniciarSesion() {

    const usuario: Usuario = {
      usuario: this.usuario,
      contrasena: this.contrasena
    }

    this.loginService.iniciarSesion(usuario)
      .subscribe(result => {
        if (result == true) {
          alert("Se ha logueado con exito");
        } else {
          alert("Usuario o Contraseña incorrectas");
        }
      });
  }

}
